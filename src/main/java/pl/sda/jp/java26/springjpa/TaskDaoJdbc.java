package pl.sda.jp.java26.springjpa;

import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TaskDaoJdbc {

    public static final String URL = "jdbc:mysql://localhost:3306/java26jpa?serverTimezone=UTC";
    private final DataSource dataSource;

    public TaskDaoJdbc() {
        dataSource = createDatasource();
    }

    public List<TaskCountByUser> getTasksCountByUser() throws SQLException {
        List<TaskCountByUser> tasks = new ArrayList<>();
        String sql = "SELECT u.username, count(t.id) cnt " +
                "FROM users u " +
                "JOIN tasks t ON t.user_id = u.id " +
                "GROUP BY u.id, u.username";

//        try (final Connection connection = getConnectionByDriverManager();
        try (final Connection connection = dataSource.getConnection();
             final Statement statement = connection.createStatement();
             final ResultSet resultSet = statement.executeQuery(sql);
        ) {
            while (resultSet.next()) {
                final String username = resultSet.getString("username");
                final int cnt = resultSet.getInt("cnt");
//            final TaskCountByUser taskCountByUser = new TaskCountByUser(username, cnt);
//            tasks.add(taskCountByUser);
                tasks.add(new TaskCountByUser(username, cnt));
            }
        }
        return tasks;
    }

    private Connection getConnectionByDriverManager() throws SQLException {
        return DriverManager
                .getConnection(URL, "jarek", "tajnehaslo");
    }

    private DataSource createDatasource() {
        final HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setJdbcUrl(URL);
        hikariDataSource.setUsername("jarek");
        hikariDataSource.setPassword("tajnehaslo");
        hikariDataSource.setMinimumIdle(2);
        hikariDataSource.setMaximumPoolSize(20);
        hikariDataSource.setIdleTimeout(5000);
        return hikariDataSource;
    }

    public User login(String username, String password) throws SQLException {
        try (final Connection connection = dataSource.getConnection();
             final Statement statement = connection.createStatement()
        ) {

            final ResultSet rs = statement.executeQuery("SELECT * " +
                    "FROM users u " +
                    "WHERE u.username = '" + username + "' " +
                    "AND u.password = '" + password + "'");

            if (rs.next()) {
                final User user = new User();
                user.setId(rs.getLong("id"));
                user.setUsername(rs.getString("username"));
                return user;
            }

        }
        return null;
    }
}
