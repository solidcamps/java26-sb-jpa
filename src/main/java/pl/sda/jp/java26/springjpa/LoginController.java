package pl.sda.jp.java26.springjpa;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;

@RestController
public class LoginController {

    @PostMapping("/login")
    public User login(@RequestParam String username, @RequestParam String password)
            throws SQLException {
        return new TaskDaoJdbc().login(username, password);
    }

}
