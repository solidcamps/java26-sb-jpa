package pl.sda.jp.java26.springjpa;

import lombok.Getter;

@Getter
public class TestDto {
    private String a;
    private Long b;

    public TestDto(String a, Long b) {
        this.a = a;
        this.b = b;
    }
}


