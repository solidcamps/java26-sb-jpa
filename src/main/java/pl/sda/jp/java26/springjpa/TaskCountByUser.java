package pl.sda.jp.java26.springjpa;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//@AllArgsConstructor
public class TaskCountByUser {
    private String name;
    private long tasksCount;

    public TaskCountByUser(String name, long tasksCount) {
        this.name = name;
        this.tasksCount = tasksCount;
    }
}
