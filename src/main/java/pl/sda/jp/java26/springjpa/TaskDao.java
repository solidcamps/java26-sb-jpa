package pl.sda.jp.java26.springjpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskDao {
    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY =
            Persistence.createEntityManagerFactory("my-persistence-unit");

    public void importInitialData() {
        final EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();

        final TodoTask todoTask = new TodoTask();
        todoTask.setTaskName("Moje pierwsze zadanie");

        entityManager.getTransaction().begin();
        entityManager.persist(todoTask);
        entityManager.getTransaction().commit();

        entityManager.close();
    }

    public void importTasksWithUsers() {
        final EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();

        final User user1 = new User("John");
        final User user2 = new User("Jerry");

        final TodoTask todoTask1 = new TodoTask();
        todoTask1.setTaskName("Zadanie 1");
        todoTask1.setUser(user1);

        final TodoTask todoTask2 = new TodoTask();
        todoTask2.setTaskName("Fajne zadanie");
        todoTask2.setUser(user2);
        final TodoTask todoTask3 = new TodoTask();
        todoTask3.setTaskName("Drugie fajne zadanie");
        todoTask3.setUser(user2);

        entityManager.getTransaction().begin();

//        entityManager.persist(user1);
        entityManager.persist(todoTask1);
        entityManager.persist(todoTask2);
        entityManager.persist(todoTask3);

        entityManager.getTransaction().commit();
        entityManager.close();


    }

    public List<TodoTask> getAllTasks() {
        final EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();

//        String jpqlQuery = "SELECT t FROM TodoTask t";
//        entityManager.createQuery(jpqlQuery);

        final List<TodoTask> tasks = entityManager
                .createQuery("SELECT t FROM TodoTask t", TodoTask.class)
                .getResultList();

        return tasks;
    }

    public List<TaskCountByUser> getTasksCountByUser() {
        final EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();

        final List<TodoTask> tempTasks = entityManager
                .createQuery("SELECT t FROM TodoTask t " +
                        "JOIN FETCH t.user " +
                        "WHERE t.user is not null", TodoTask.class)
                .getResultList();
        List<TestDto> tempDtos = new ArrayList<>();
        for (TodoTask tempTask : tempTasks) {
            tempTask.getUser().getUsername();
            final TestDto x = new TestDto(tempTask.getTaskName(), tempTask.getId());
            tempDtos.add(x);
        }

        System.out.println("tempDtos: " + tempDtos);

        final List<TestDto> resultList = entityManager
                .createQuery("SELECT new pl.sda.jp.java26.springjpa.TestDto(t.taskName, t.id) " +
                        "FROM TodoTask t ", TestDto.class)
                .getResultList();

//        ((List<TestDto>)resultList).get(0).getA();
//        ((TestDto)resultList.get(0)).getA();
//        ((String)((List<Object[]>)resultList).get(0)[0]).startsWith("a");

        System.out.println("resultList: " + resultList);

        return entityManager
                .createQuery("SELECT " +
                                "new pl.sda.jp.java26.springjpa.TaskCountByUser(t.user.username, count(t)) " +
                                "FROM TodoTask t " +
                                "GROUP BY t.user",
                        TaskCountByUser.class)
                .getResultList();


//        select user1_.username as col_0_0_, count(todotask0_.id) as col_1_0_
//        from tasks todotask0_
//        cross join users user1_
//        where todotask0_.user_id=user1_.id
//        group by todotask0_.user_id

//        return entityManager
//                .createQuery("SELECT " +
//                                "new pl.sda.jp.java26.springjpa.TaskCountByUser(u.username, count(t)) " +
//                                "FROM TodoTask t " +
//                                "JOIN t.user u " +
//                                "GROUP BY u",
//                        TaskCountByUser.class)
//                .getResultList();
//        user1_.username as col_0_0_, count(todotask0_.id) as col_1_0_
//        from tasks todotask0_
//        inner join users user1_ on todotask0_.user_id=user1_.id
//        group by user1_.id
    }

    public List<TaskCountByUser> getTasksCountByUserWithPlainJava() {
        final EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        final List<TodoTask> tasks = entityManager.createQuery("SELECT t FROM TodoTask t WHERE t.user is not null", TodoTask.class)
                .getResultList();

        final List<User> users = entityManager.createQuery("SELECT u FROM User u", User.class)
                .getResultList();

        int userCounter = 0, taskCounter = 0, additionalLoopsCounter = 0;
        Map<User, Integer> userTasksCountMap = new HashMap<>();
        for (User user : users) {
            userCounter++;
            List<TodoTask> currentUserTasks = new ArrayList<>();
            for (TodoTask task : tasks) {
                taskCounter++;
                if (/*task.getUser() != null && */user.getId().equals(task.getUser().getId())) {
                    currentUserTasks.add(task);
                }
            }
            userTasksCountMap.put(user, currentUserTasks.size());
        }

        List<TaskCountByUser> tasksCountByUser = new ArrayList<>();
        for (User user : userTasksCountMap.keySet()) {
            additionalLoopsCounter++;
            int values = userTasksCountMap.get(user);
            tasksCountByUser.add(new TaskCountByUser(user.getUsername(), values));
        }

        System.out.println(
                String.format("Counters: users:%d, tasks:%d, other:%d",
                        userCounter, taskCounter, additionalLoopsCounter)
        );
        return tasksCountByUser;
    }

    public TodoTask createNewTask(String taskName, String userName) {
        final TodoTask todoTask = new TodoTask();
        todoTask.setTaskName(taskName);

        final EntityManager entityManager = ENTITY_MANAGER_FACTORY.createEntityManager();
        final List<User> foundUsers = entityManager
                .createQuery("SELECT u FROM User u WHERE u.username = :uname", User.class)
                .setParameter("uname", userName)
                .getResultList();

        entityManager.getTransaction().begin();
        if (foundUsers.isEmpty()) {
            final User user = new User(userName);
//            entityManager.persist(user);
            todoTask.setUser(user);
        } else {
            final User user = foundUsers.get(0);
            todoTask.setUser(user);
        }

        entityManager.persist(todoTask);
        entityManager.getTransaction().commit();
        entityManager.close();

        return todoTask;
    }
}
