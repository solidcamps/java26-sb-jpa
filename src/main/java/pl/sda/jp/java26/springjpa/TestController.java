package pl.sda.jp.java26.springjpa;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;

@RestController
public class TestController {

    private final TaskDao taskDao/* = new TaskDao()*/;
    private final TaskDaoJdbc taskDaoJdbc;

    public TestController() {
        this.taskDao = new TaskDao();
        this.taskDaoJdbc = new TaskDaoJdbc();
    }

    @GetMapping("/test")
    public String test() {
        return "Hello test";
    }

    @GetMapping("/TEST")
    public String test2() {
        return "Hello TEST";
    }

    @PostMapping("/task")
    public TodoTask createNewTask(@RequestParam String taskName,
                                  @RequestParam String userName) {
        System.out.println("taskName: " + taskName);
        System.out.println("userName: " + userName);
        //todo: save new Task for given User - if User doesn't exist, create new one.
        return taskDao.createNewTask(taskName, userName);
    }

    @PostMapping("/import")
    public String initialImport() {
        taskDao.importInitialData();
        return "OK";
    }

    @PostMapping("/importWithUsers")
    public String initialImportWithUsers() {
        taskDao.importTasksWithUsers();
        return "OK";
    }

    @GetMapping("/tasks")
    public List<TodoTask> getTasks() {
        final List<TodoTask> allTasks = taskDao.getAllTasks();
        return allTasks;
    }

    @GetMapping("/tasks/countByUser")
    public List<TaskCountByUser> getTasksCountByUser() {
        return taskDao.getTasksCountByUser();
    }

    @GetMapping("/tasks/countByUserJava")
    public List<TaskCountByUser> getTasksCountByUserJava() {
        return taskDao.getTasksCountByUserWithPlainJava();
    }

    @GetMapping("/tasks/countByUserJdbc")
    public List<TaskCountByUser> getTasksCountByUserJdbc() throws SQLException {
        return taskDaoJdbc.getTasksCountByUser();
    }

}
